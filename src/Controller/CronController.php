<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Cache\Cache;
use Cake\Log\Log;

class CronController extends AppController {

    private $allowsIP = false;
    private $maxTimeRun = 285; //Maksymalny czas (w sekundach) wykonania crona jeżeli wykonywana jest kolejna pętla a czas od uruchomienia przekroczył ten czas to cron kończy swoje działanie.
    private $cronItem = null;
    private $countLoop = 0;
    private $webserwisTemplate = [];
    private $trimAddress = [" " => "", "\t" => "", "\n" => "", "\r" => "", "-" => "", "," => "", "/" => ""];
    private $timeStart;
    private $blockedTimeMin;
    private $blockedTimeMax;
    private $blockLog = true;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['run']);
        date_default_timezone_set('Europe/Warsaw');
        ignore_user_abort(true);
        set_time_limit(0);
        ini_set('max_execution_time', 900);
        ini_set('max_input_time', 900);
        ini_set('mysql.connect_timeout', 900);
        ini_set('max_input_vars', 10000);
        ini_set('default_socket_timeout', 900);
        if (empty($this->WebserwisTemplate)) {
            $this->loadModel('WebserwisTemplate');
        }
        $this->webserwisTemplate = $this->WebserwisTemplate->find('list', ['keyField' => 'field', 'valueField' => 'mask', 'groupField' => 'table_name'])->toArray();
        $this->blockedTimeMin = strtotime(date('Y-m-d 14:00:00'));
        $this->blockedTimeMax = strtotime(date('Y-m-d 16:00:00'));
    }

    /**
     * Uruchomienie crona
     * @param type $id
     * @return type
     */
    public function run($id) {
        $this->autoRender = false;
        if (empty($id)) {
            die('Brak ID crona');
        }
        if (!empty($this->allowsIP)) {
            if (array_search($this->request->clientIp(), $this->allowsIP) === false) {
                die('Access denied');
            }
        }
        $this->cronItem = $this->Cron->find('all')->where(['Cron.id' => $id, 'OR' => ['Cron.blokada' => 0, 'Cron.blokada IS' => NULL]])->first();
        if (empty($this->cronItem)) {
            die();
        }
        if (!empty($this->cronItem->log)) {
            $this->blockLog = false;
        }
        if ($this->cronItem->status > 0) {
            if ((!empty($this->cronItem->last_run) && (time() - strtotime($this->cronItem->last_run->format('Y-m-d H:i:s'))) >= 3600)) {
                if (!$this->blockLog) {
                    Log::write('info', __('Cron {0} reset. Nie dziala od {1} sec.', [$id, (time() - strtotime($this->cronItem->last_run->format('Y-m-d H:i:s')))]), ['cronInfo']);
                }
                $this->cronItem->set('status', 0);
            } else {
                //'Cron jest w trakcie dzialania'
                die();
            }
        }
        if (!empty($this->cronItem->max_time)) {
            $this->maxTimeRun = $this->cronItem->max_time;
        }
        $this->timeStart = time();
        $timeEnd = $this->timeStart;
        $token = uniqid();
        $up = ['token' => $token, 'last_run' => date('Y-m-d H:i:s'), 'status' => 1];
        $this->Cron->updateAll($up, ['id' => $id]);
        $brake = false;
        $finished = false;
        $pause = false;
        do {
            $this->countLoop++;
            if (empty($this->cronItem)) {
                break;
            }
            if ($this->cronItem->status != 1) {
                $this->cronItem->set('status', 1);
                $this->Cron->save($this->cronItem);
            }
            $skip = $this->cronItem->skip;
            $take = $this->cronItem->take;
            if ($this->cronItem->page > 0) {
                $skip = ($this->cronItem->take * $this->cronItem->page) + 1;
                $take = $take - 1;
            }
            $modifiedTime = (!empty($this->cronItem->modifiedTime) ? $this->cronItem->modifiedTime->format('Y-m-d') : null);
            switch ($this->cronItem->typ) {
                case 'allUsers': {
                        if (time() >= $this->blockedTimeMin && time() <= $this->blockedTimeMax) {
                            $result = ['status' => 'pause', 'message' => 'Ten cron nie może wykonywać się o ' . date('H:i:s')];
                            $pause = true;
                        } else {
                            if (empty($this->cronItem->page)) {
                                $this->cronItem->set('nextModifiedTime', date('Y-m-d'));
                            }
                            $result = $this->getUsers($skip, $take, $this->cronItem->page, $modifiedTime);
                        }
                    }break;
                default : {
                        $result = ['status' => 'error', 'message' => 'Brak poprawnej akcji.'];
                    }break;
            }
            $timeEnd = time();

            if ($result['status'] == 'pause') {
                $brake = true;
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', __('Cron {0} zakończono w {1} sec. {2}', [$id, ($timeEnd - $this->timeStart), $result['message']]), ['cronInfo']);
                }
            } elseif ($result['status'] == 'stop') {
                $brake = true;
                $finished = true;
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', __('Cron {0} zakończono w {1} sec. {2}', [$id, ($timeEnd - $this->timeStart), $result['message']]), ['cronInfo']);
                }
            } elseif ($result['status'] == 'success') {
                if (!$this->blockLog) {
                    Log::write('info', __('Cron {0} success. {1}', [$id, $result['message']]), ['cronInfo']);
                }
                sleep(3);
            } elseif ($result['status'] == 'continue') {
                if ((!key_exists('log', $result) || $result['log'] == true) && !$this->blockLog) {
                    Log::write('info', $result['message'], ['cronInfo']);
                }
                unset($result);
                sleep(3);
            } else {
                $brake = true;
                Log::write('error', __('Cron {0} error. {1}', [$id, $result['message']]), ['cronError']);
            }
            if ($brake) {
                break;
            }
        } while (($timeEnd - $this->timeStart) < $this->maxTimeRun);
        $this->dbReconnect();
        $endTime = ($timeEnd - $this->timeStart);
        $message = __('{3} Cron job {0} time: {1} sec. {2}', [$id, $endTime, 'Memory usage: ' . (memory_get_usage(true) / 1024 / 1024) . ' MB', date('Y-m-d H:i:s')]);
        $this->cronItem->set('status', 0);
        if (($finished || ($this->countLoop == 1 && $endTime < $this->maxTimeRun)) && !$pause) {
            $this->cronItem->set('page', 0);
            if (!empty($this->cronItem->nextModifiedTime)) {
                $this->cronItem->set('modifiedTime', $this->cronItem->nextModifiedTime);
            }
        }
        $this->Cron->save($this->cronItem);
        $this->response->type('json');
        $this->response->body($message);
        return $this->response;
    }

    /**
     * Pobranie klientów z webserwisu
     * @param type $skip
     * @param type $take
     * @param type $cronPage
     * @param type $modifiedTime
     * @return type
     */
    private function getUsers($skip, $take, $cronPage = 0, $modifiedTime = null) {
        if (empty($this->Uzytkownik)) {
            $this->loadModel('Uzytkownik');
        }
        $service = new \Webservice\Webservice();
        $loginStatus = $service->login();
        $error = true;
        $countGet = 0;
        $stop = false;
        if (is_array($loginStatus) && $loginStatus['status'] == 'success') {
            $this->dbReconnect();
            $allUsers = [];
            $serviceData = $service->getUsers($skip, $take, $modifiedTime);
            $users = null;
            $this->dbReconnect();
            if ($serviceData['status'] == 'success') {
                if (!$this->blockLog) {
                    $cacheLog = Cache::read('api_users');
                    $cacheLog['success'][date('Y-m-d H:i:s')] = $serviceData;
                    Cache::write('api_users', $cacheLog);
                }
                $error = false;
                $users = $serviceData['results'];
                if (!empty($users)) {
                    if (property_exists($users, 'Contractor')) {
                        if (is_array($users->Contractor->Contractors)) {
                            $countGet = count($users->Contractor->Contractors);
                            foreach ($users->Contractor->Contractors as $_user) {
                                $allUsers[$_user->RowNumber] = $this->setUserToArray($_user);
                            }
                        } else {
                            if (property_exists($users->Contractor, 'Contractors')) {
                                $countGet = 1;
                                $allUsers[$users->Contractor->Contractors->RowNumber] = $this->setUserToArray($users->Contractor->Contractors);
                            } else {
                                $countGet = 1;
                                $allUsers[$users->Contractor->RowNumber] = $this->setUserToArray($users->Contractor);
                            }
                        }
                    }
                    $response = 'Pobrano ' . $countGet . ' użytkowników';
                }
            } else {
                if (!$this->blockLog) {
                    $cacheLog = Cache::read('api_users');
                    $cacheLog['errors'][date('Y-m-d H:i:s')] = $serviceData;
                    Cache::write('api_users', $cacheLog);
                }
                $response = json_encode($serviceData);
            }

            if (!empty($countGet) && $countGet < $take) {
                $response = 'Pobrano ' . $countGet . ' użytkowników. Pobieranie zakończone.';
                $stop = true;
            }
            if (empty($countGet)) {
                $response = 'Brak klientów do pobrania. Pobieranie zakończone.';
                $stop = true;
            }
        } else {
            $this->dbReconnect();
            if (!empty($loginStatus['error'])) {
                $response = $loginStatus['error'];
            } else {
                $response = 'Błąd połączenia z webserwisem';
            }
            $stop = true;
        }
        $updateArr = ['status' => 2];
        if (!$error) {
            $updateArr['page'] = ($cronPage + 1);
        }
        $this->cronItem = $this->Cron->patchEntity($this->cronItem, $updateArr);
        $this->Cron->save($this->cronItem);
        if ($error) {
            return ['status' => 'error', 'message' => $response];
        } else {
            return ['status' => ($stop ? 'stop' : 'success'), 'message' => $response];
        }
    }

    /**
     * Przygotowanie danych klienta i zapis do bazy
     * @param type $_user
     * @return type
     */
    private function setUserToArray($_user) {
        $user = [
            'service_row' => $_user->RowNumber,
            'data_pobrania' => date('Y-m-d H:i:s'),
            'token' => uniqid(),
            'aktywny' => 0,
            'typ' => 'b2b',
        ];
        if (!empty($this->webserwisTemplate) && key_exists('uzytkownik', $this->webserwisTemplate) && key_exists('service_id', $this->webserwisTemplate['uzytkownik']) && property_exists($_user, $this->webserwisTemplate['uzytkownik']['service_id'])) {
            foreach ($this->webserwisTemplate['uzytkownik'] as $field => $mask) {
                switch ($field) {
                    case 'termin_splaty' : {
                            if (property_exists($_user, $mask)) {
                                if (!empty(strtotime($_user->{$mask})) && strtotime($_user->{$mask}) > 0) {
                                    $user[$field] = date('Y-m-d', strtotime($_user->{$mask}));
                                } else {
                                    $user[$field] = null;
                                }
                            } else {
                                $user[$field] = null;
                            }
                        } break;
                    case 'data_rejestracji' : {
                            if (property_exists($_user, $mask)) {
                                $user[$field] = date('Y-m-d', strtotime($_user->{$mask}));
                            }
                        } break;
                    default : {
                            if (property_exists($_user, $mask)) {
                                $user[$field] = $_user->{$mask};
                            }
                        } break;
                }
            }
        }
        $adresDefaultKey = mb_strtolower(trim(str_replace(array_keys($this->trimAddress), array_values($this->trimAddress), trim($user['dane']))));
        $userEntity = $this->Uzytkownik->find('all', ['contain' => ['UzytkownikSubKonto', 'UzytkownikAdres']])->where(['Uzytkownik.service_id' => $user['service_id']])->first();

        if (empty($userEntity)) {
            $user['password'] = $this->Txt->generatePassword(8);
        } else {
            if (!empty($userEntity->aktywny)) {
                $user['aktywny'] = 1;
            }
            if (!empty($userEntity->token)) {
                unset($user['token']);
            }
        }
        $addresses = [];
        $addressIds = [];
        if (!empty($_user->Addresses)) {
            $i = 0;
            if (is_array($_user->Addresses->Addresses)) {
                foreach ($_user->Addresses->Addresses as $address) {
                    $addresses[$i] = $this->userAddressToArray($address, $user, $adresDefaultKey);
                    if (!empty($userEntity)) {
                        $addresses[$i]['uzytkownik_id'] = $userEntity->id;
                    }
                    $addressIds[$address->UniqueIdentifier] = $i;
                    $i++;
                }
            } else {
                $address = $_user->Addresses->Addresses;
                $addresses[$i] = $this->userAddressToArray($address, $user, $adresDefaultKey);
                if (!empty($userEntity)) {
                    $addresses[$i]['uzytkownik_id'] = $userEntity->id;
                }
                $addressIds[$address->UniqueIdentifier] = $i;
            }
        }
        if (!empty($addresses)) {
            if (!empty($userEntity) && !empty($userEntity->uzytkownik_adres)) {
                foreach ($userEntity->uzytkownik_adres as $uzytkownikAdres) {
                    if (!empty($uzytkownikAdres->service_id) && key_exists($uzytkownikAdres->service_id, $addressIds)) {
                        $addresses[$addressIds[$uzytkownikAdres->service_id]]['id'] = $uzytkownikAdres->id;
                    }
                }
            }
            $user['uzytkownik_adres'] = $addresses;
        }
        $subUsers = [];
        $subAcountNames = [];
        $userSubInfolinia = null;
        if (!empty($_user->Subaccounts)) {
            $i = 0;
            if (is_array($_user->Subaccounts->Subaccounts)) {
                foreach ($_user->Subaccounts->Subaccounts as $subAccount) {

                    if (key_exists($subAccount->Name, $subAcountNames)) {
                        continue;
                    }
                    $subUsers[$i] = $this->userSubKontoToArray($subAccount, $user['service_id']);
                    if (!empty($userEntity)) {
                        $subUsers[$i]['uzytkownik_id'] = $userEntity->id;
                    }
                    if (empty($user['infolinia']) && empty($userSubInfolinia) && !empty($subUsers[$i]['infolinia'])) {
                        $userSubInfolinia = $subUsers[$i]['infolinia'];
                    }
                    $subAcountNames[trim($subUsers[$i]['nazwa'])] = $i;
                    $i++;
                }
            } else {
                $subAccount = $_user->Subaccounts->Subaccounts;
                if (!key_exists($subAccount->Name, $subAcountNames)) {
                    $subUsers[$i] = $this->userSubKontoToArray($subAccount, $user['service_id']);
                    if (!empty($userEntity)) {
                        $subUsers[$i]['uzytkownik_id'] = $userEntity->id;
                    }
                    if (empty($user['infolinia']) && empty($userSubInfolinia) && !empty($subUsers[$i]['infolinia'])) {
                        $userSubInfolinia = $subUsers[$i]['infolinia'];
                    }
                    $subAcountNames[trim($subUsers[$i]['nazwa'])] = $i;
                }
            }
        }
        if (!empty($subUsers)) {
            if (!empty($userEntity) && !empty($userEntity->uzytkownik_sub_konto)) {
                foreach ($userEntity->uzytkownik_sub_konto as $subKonto) {
                    if (key_exists($subKonto->nazwa, $subAcountNames)) {
                        $subUsers[$subAcountNames[$subKonto->nazwa]]['id'] = $subKonto->id;
                        if (empty($subUsers[$subAcountNames[$subKonto->nazwa]]['email'])) {
                            $subUsers[$subAcountNames[$subKonto->nazwa]]['email'] = $subKonto->email;
                        }
                        if (empty($subUsers[$subAcountNames[$subKonto->nazwa]]['telefon'])) {
                            $subUsers[$subAcountNames[$subKonto->nazwa]]['telefon'] = $subKonto->telefon;
                        }
                        if (!empty($subKonto->username)) {
                            unset($subUsers[$subAcountNames[$subKonto->nazwa]]['username']);
                        }
                        unset($subUsers[$subAcountNames[$subKonto->nazwa]]['password'], $subUsers[$subAcountNames[$subKonto->nazwa]]['aktywny']);
                    } else {
                        $subKonto->aktywny = 0;
                    }
                }
            }
            $user['uzytkownik_sub_konto'] = $subUsers;
        }
        if (empty($userEntity)) {
            $userEntity = $this->Uzytkownik->newEntity();
        }
        if (empty($user['infolinia']) && !empty($userSubInfolinia)) {
            $user['infolinia'] = $userSubInfolinia;
        }
        if (!empty($user['service_id'])) {
            $userEntity = $this->Uzytkownik->patchEntity($userEntity, $user);
            if (!$this->Uzytkownik->save($userEntity, ['validate' => 'webserwis'])) {
                $err = $userEntity->errors();
                var_dump($err, $userEntity);
                if (!empty($err['uzytkownik_sub_konto'])) {
                    var_dump($err['uzytkownik_sub_konto']);
                }
            }
        }
        return $user;
    }

    /**
     * Przygotowanie tablicy adresu
     * @param type $address
     * @param type $user
     * @param type $adresDefaultKey
     * @return type array
     */
    private function userAddressToArray($address, $user = null, $adresDefaultKey = null) {
        if (empty($address)) {
            return null;
        }
        $userAddr = [];
        if (!empty($this->webserwisTemplate) && key_exists('uzytkownik_adres', $this->webserwisTemplate)) {
            foreach ($this->webserwisTemplate['uzytkownik_adres'] as $field => $mask) {
                if (property_exists($address, $mask)) {
                    $userAddr[$field] = $address->{$mask};
                }
            }
        }
        if (!key_exists('typ', $userAddr)) {
            $addressCheckKey = $userAddr['nazwa'] . $userAddr['kod'] . $userAddr['miasto'] . $userAddr['ulica'];
            $addressCheckKey = mb_strtolower(trim(str_replace(array_keys($this->trimAddress), array_values($this->trimAddress), $addressCheckKey)));
            $userAddr['typ'] = (($addressCheckKey == $adresDefaultKey) ? 2 : 1);
        }
        if (!empty($user) && $userAddr['typ'] == 2 && (!key_exists('nip', $userAddr) || empty($userAddr['nip']))) {
            $userAddr['nip'] = $user['nip'];
        }
        return $userAddr;
    }

    /**
     * Przygotowanie tablicy subkonta
     * @param type $subAcount
     * @param type $parentId
     * @return type array
     */
    private function userSubKontoToArray($subAcount, $parentId = null) {
        if (empty($subAcount)) {
            return null;
        }
        $konto = [
            'aktywny' => 0,
            'password' => $this->Txt->generatePassword(8),
            'username' => $this->Txt->getSubkonoUsername($parentId, 5)
        ];
        if (!empty($this->webserwisTemplate) && key_exists('uzytkownik_sub_konto', $this->webserwisTemplate)) {
            foreach ($this->webserwisTemplate['uzytkownik_sub_konto'] as $field => $mask) {
                if (property_exists($subAcount, $mask)) {
                    $konto[$field] = trim($subAcount->{$mask});
                }
            }
        }
        return $konto;
    }

}
